from flask import Flask, render_template, request
from pyowm import OWM
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import io
import base64
import utility
import numpy as np
import time

API_Key = '4b02792e03d8af53dfdfc68ad9541b3a'
owm = OWM(API_Key)

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')
    else:
        location = request.form['location']
        forcast = calculateWeatherForcast(location)
        emailId = ''
        if 'emailId' in request.form:
            emailId = request.form['emailId']
        if emailId != '':
            utility.send_weather_forcast_email(emailId, forcast['hourly'], forcast['daily'])
        return render_template('index.html', location=location, forcast=forcast)


def calculateWeatherForcast(location):
    daily_weather_prediction = []
    days_info = []
    my_date = datetime.now() - timedelta(days=1)
    fc = owm.daily_forecast(location, limit=6)
    f = fc.get_forecast()
    lst = f.get_weathers()
    for weather in lst:
        daily_weather_prediction.append(weather.get_temperature('celsius')['day'])
        my_date = my_date + timedelta(days=1)
        days_info.append(str(my_date.strftime("%d %B")))
    img = io.BytesIO()
    y = daily_weather_prediction
    x = [1, 2, 3, 4, 5, 6]
    plt.plot(x, y, marker='o', color='b')
    plt.xticks(x, days_info)
    plt.xlabel('Day')
    dailyFileName = str(time.time()) + '.png'
    plt.ylabel('Tempreture Degree Celcius')
    plt.savefig('static/images/' + dailyFileName, format='png')
    img.seek(0)
    daily_forecast_plot = dailyFileName
    plt.clf()
    plt.cla()
    plt.close()
    my_hours = datetime.now() - timedelta(hours=3)
    hourly_weather_prediction = []
    hours_info = []
    fc2 = owm.three_hours_forecast(location)
    f2 = fc2.get_forecast()
    lst2 = f2.get_weathers()
    lst2 = lst2[0:6]
    for weather in lst2:
        hourly_weather_prediction.append(weather.get_temperature('celsius')['temp'])
        my_hours = my_hours + timedelta(hours=3)
        hours_info.append(str(my_hours.strftime("%H : %M")))
    img2 = io.BytesIO()
    y = hourly_weather_prediction
    x = [1, 2, 3, 4, 5, 6]
    plt.plot(x, y, marker='o', color='b')
    plt.xticks(x, hours_info)
    plt.xlabel('Hours')
    plt.ylabel('Tempreture Degree Celcius')
    hourlyFileName = str(time.time()) + '.png'
    plt.savefig('static/images/' + hourlyFileName, format='png')
    img2.seek(0)
    hourly_forecast_plot = hourlyFileName
    plt.clf()
    plt.cla()
    plt.close()
    forcast = {}
    forcast.setdefault('daily', daily_forecast_plot)
    forcast.setdefault('hourly', hourly_forecast_plot)
    forcast.setdefault('hourly_mean', round(np.mean(hourly_weather_prediction), 2))
    forcast.setdefault('daily_mean', round(np.mean(daily_weather_prediction), 2))
    forcast.setdefault('forcasted', True)
    return forcast


if __name__ == "__main__":
    app.run()
